pub mod led;
pub mod side;

use self::side::Plane;
use alloc::vec::Vec;
use core::fmt::Display;
use led::Led;
use side::Side;
use smart_leds::RGB8;

#[derive(Default)]
pub struct Cube {
    // side connection order: back -> bottom -> right -> top -> left -> front
    pub front: Side,
    pub back: Side,
    pub top: Side,
    pub bottom: Side,
    pub left: Side,
    pub right: Side,
}

impl Cube {
    pub fn new() -> Self {
        let front = Side::new_on_plane(Plane::XZ, side::PCB_LED_OFFSET, false);
        let back = Side::new_on_plane(Plane::XZ, -side::PCB_LED_OFFSET, true);
        let top = Side::new_on_plane(Plane::XY, side::PCB_LED_OFFSET, false);
        let bottom = Side::new_on_plane(Plane::XY, -side::PCB_LED_OFFSET, true);
        let left = Side::new_on_plane(Plane::YZ, -side::PCB_LED_OFFSET, true);
        let right = Side::new_on_plane(Plane::YZ, side::PCB_LED_OFFSET, false);

        Self {
            front,
            back,
            top,
            bottom,
            left,
            right,
        }
    }

    pub fn set_color(&mut self, color: RGB8) {
        for side in [
            &mut self.front,
            &mut self.back,
            &mut self.bottom,
            &mut self.top,
            &mut self.left,
            &mut self.right,
        ] {
            side.set_color(color);
        }
    }

    pub fn all_leds(&self) -> Vec<&Led> {
        let v = vec![
            self.back.all_leds(),
            self.bottom.all_leds(),
            self.right.all_leds(),
            self.top.all_leds(),
            self.left.all_leds(),
            self.front.all_leds(),
        ];
        v.into_iter().flatten().collect()
    }

    pub fn all_leds_mut(&mut self) -> Vec<&mut Led> {
        let v = vec![
            self.back.all_leds_mut(),
            self.bottom.all_leds_mut(),
            self.right.all_leds_mut(),
            self.top.all_leds_mut(),
            self.left.all_leds_mut(),
            self.front.all_leds_mut(),
        ];
        v.into_iter().flatten().collect()
    }
}

impl Display for Cube {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        writeln!(f, "FRONT:\n{}", self.front)?;
        writeln!(f, "BACK:\n{}", self.back)?;
        writeln!(f, "TOP:\n{}", self.top)?;
        writeln!(f, "BOTTOM:\n{}", self.bottom)?;
        writeln!(f, "LEFT:\n{}", self.left)?;
        writeln!(f, "RIGHT:\n{}", self.right)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display_default() {
        let cube = Cube::default();
        println!("{}", cube);
    }

    #[test]
    fn display() {
        let cube = Cube::new();
        println!("{}", cube);
    }

    #[test]
    fn all_leds() {
        let cube = Cube::new();
        for (i, led) in cube.all_leds().iter().enumerate() {
            println!("{:2}:{}", i, led);
        }
    }

    #[test]
    fn all_colors() {
        let cube = Cube::new();
        for (i, color) in cube.all_colors().iter().enumerate() {
            println!("{:2}:{}", i, color);
        }
    }
}
