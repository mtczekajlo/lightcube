#![cfg_attr(not(test), no_std)]
#![feature(slice_flatten)]
#![allow(dead_code)]

#[macro_use]
extern crate alloc;

pub mod cube;
