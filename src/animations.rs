#![allow(dead_code)]

extern crate alloc;
use alloc::vec::Vec;
use libm::fabsf;
use lightcube_libs::cube::led::Led;
use lsm6ds3tr::XYZ;
use rand::{rngs::SmallRng, RngCore};
use smart_leds::RGB8;

pub fn rotation_color(xyz: &XYZ<f32>, leds: &mut Vec<&mut Led>) {
    let x = ((fabsf(xyz.x.clamp(-1.0, 1.0))) * 255.0) as u8;
    let y = ((fabsf(xyz.y.clamp(-1.0, 1.0))) * 255.0) as u8;
    let z = ((fabsf(xyz.z.clamp(-1.0, 1.0))) * 255.0) as u8;

    let sum = x as u16 + y as u16 + z as u16;
    let factor = 255.0 / (sum as f32);

    let r = (z as f32 * factor) as u8;
    let g = (y as f32 * factor) as u8;
    let b = (x as f32 * factor) as u8;

    let color: RGB8 = (r, g, b).into();

    for led in leds {
        led.color = color;
    }
}

fn wheel_color(mut wheel_pos: u8) -> RGB8 {
    wheel_pos = 255 - wheel_pos;
    if wheel_pos < 85 {
        return (255 - wheel_pos * 3, 0, wheel_pos * 3).into();
    }
    if wheel_pos < 170 {
        wheel_pos -= 85;
        return (0, wheel_pos * 3, 255 - wheel_pos * 3).into();
    }
    wheel_pos -= 170;
    (wheel_pos * 3, 255 - wheel_pos * 3, 0).into()
}

pub fn rotation_wheel(xyz: &XYZ<f32>, leds: &mut [&mut Led]) {
    let x = ((fabsf(xyz.x.clamp(-1.0, 1.0))) * 255.0) as u8;
    // let y = ((fabsf(xyz.y.clamp(-1.0, 1.0))) * 255.0) as u8;
    // let z = ((fabsf(xyz.z.clamp(-1.0, 1.0))) * 255.0) as u8;
    let len = leds.len();
    for (i, led) in leds.iter_mut().enumerate() {
        led.color = wheel_color((((i * 256) as u16 / len as u16 + x as u16) & 255) as u8);
    }
}

pub fn wheel(position: u8, leds: &mut [&mut Led]) {
    let len = leds.len() as u16;
    let position = (position as f32 * 256_f32 / len as f32) as u8;
    for (i, led) in leds.iter_mut().enumerate() {
        led.color = wheel_color((((i * 256) as u16 / len + position as u16) & 255) as u8);
    }
}

pub fn random(small_rng: &mut SmallRng, leds: &mut Vec<&mut Led>) {
    for led in leds {
        let r: u8 = small_rng.next_u32() as u8;
        let g: u8 = small_rng.next_u32() as u8;
        let b: u8 = small_rng.next_u32() as u8;
        led.color = (r, g, b).into();
    }
}

pub fn random_rgb(small_rng: &mut SmallRng, leds: &mut Vec<&mut Led>) {
    for led in leds {
        let color: u8 = small_rng.next_u32() as u8 % 3;
        let mut r: u8 = 0;
        let mut g: u8 = 0;
        let mut b: u8 = 0;
        match color {
            0 => r = 255,
            1 => g = 255,
            2 => b = 255,
            _ => (),
        };
        led.color = (r, g, b).into();
    }
}

pub fn random_rgb_brightness(small_rng: &mut SmallRng, leds: &mut Vec<&mut Led>) {
    for led in leds {
        let color: u8 = small_rng.next_u32() as u8 % 3;
        let mut r: u8 = 0;
        let mut g: u8 = 0;
        let mut b: u8 = 0;
        match color {
            0 => r = small_rng.next_u32() as u8,
            1 => g = small_rng.next_u32() as u8,
            2 => b = small_rng.next_u32() as u8,
            _ => (),
        };
        led.color = (r, g, b).into();
    }
}

pub fn random_rgbcmy_brightness(small_rng: &mut SmallRng, leds: &mut Vec<&mut Led>) {
    for led in leds {
        let color: u8 = small_rng.next_u32() as u8 % 6;
        let mut r: u8 = 0;
        let mut g: u8 = 0;
        let mut b: u8 = 0;
        match color {
            0 => r = small_rng.next_u32() as u8,
            1 => g = small_rng.next_u32() as u8,
            2 => b = small_rng.next_u32() as u8,
            3 => {
                r = small_rng.next_u32() as u8;
                g = small_rng.next_u32() as u8;
            }
            4 => {
                r = small_rng.next_u32() as u8;
                b = small_rng.next_u32() as u8;
            }
            5 => {
                g = small_rng.next_u32() as u8;
                b = small_rng.next_u32() as u8;
            }
            _ => (),
        };
        led.color = (r, g, b).into();
    }
}

pub fn random_rgbcmy(small_rng: &mut SmallRng, leds: &mut Vec<&mut Led>) {
    for led in leds {
        let color: u8 = small_rng.next_u32() as u8 % 6;
        let mut r: u8 = 0;
        let mut g: u8 = 0;
        let mut b: u8 = 0;
        match color {
            0 => r = 255,
            1 => g = 255,
            2 => b = 255,
            3 => {
                r = 255;
                g = 255;
            }
            4 => {
                r = 255;
                b = 255;
            }
            5 => {
                g = 255;
                b = 255;
            }
            _ => (),
        };
        led.color = (r, g, b).into();
    }
}

pub fn dot(i: usize, leds: &mut [&mut Led]) {
    for led in leds.iter_mut() {
        led.color = (0, 0, 0).into();
    }
    leds[i].color = (255, 255, 255).into();
}

pub fn color_dot(small_rng: &mut SmallRng, leds: &mut [&mut Led]) {
    for led in leds.iter_mut() {
        led.color = (0, 0, 0).into();
    }
    let r: u8 = small_rng.next_u32() as u8;
    let g: u8 = small_rng.next_u32() as u8;
    let b: u8 = small_rng.next_u32() as u8;
    let i = (small_rng.next_u32() % leds.len() as u32) as usize;
    leds[i].color = (r, g, b).into();
}

pub fn random_dot(small_rng: &mut SmallRng, leds: &mut [&mut Led]) {
    dot((small_rng.next_u32() % leds.len() as u32) as usize, leds);
}

pub fn random_color_dot(small_rng: &mut SmallRng, leds: &mut [&mut Led]) {
    color_dot(small_rng, leds);
}
