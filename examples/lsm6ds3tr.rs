#![no_std]
#![no_main]

use alloc::format;
use alloc::string::String;
use defmt::*;
use defmt_rtt as _;
use embedded_alloc::Heap;
use fugit::RateExtU32;
use lsm6ds3tr::{
    interface::SpiInterface, InterruptRoute, IrqSettings, LsmSettings, TapRecognitionMode,
    LSM6DS3TR, XYZ,
};
use panic_halt as _;
use rp_pico::entry;
use rp_pico::hal;
use rp_pico::hal::pac;
use rp_pico::hal::prelude::*;

extern crate alloc;
#[global_allocator]
static HEAP: Heap = Heap::empty();

#[entry]
fn main() -> ! {
    {
        use core::mem::MaybeUninit;
        const HEAP_SIZE: usize = 1024;
        static mut HEAP_MEM: [MaybeUninit<u8>; HEAP_SIZE] = [MaybeUninit::uninit(); HEAP_SIZE];
        unsafe { HEAP.init(HEAP_MEM.as_ptr() as usize, HEAP_SIZE) }
    }

    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let _spi_sclk = pins.gpio0.into_mode::<hal::gpio::FunctionSpi>();
    let spi_cs = pins.gpio1.into_push_pull_output();
    let _spi_miso = pins.gpio2.into_mode::<hal::gpio::FunctionSpi>();
    let _spi_mosi = pins.gpio3.into_mode::<hal::gpio::FunctionSpi>();
    let spi = hal::Spi::<_, _, 8>::new(pac.SPI0);

    let spi = spi.init(
        &mut pac.RESETS,
        clocks.peripheral_clock.freq(),
        8.MHz(),
        &embedded_hal::spi::MODE_0,
    );

    let spi_interface = SpiInterface::new(spi, spi_cs);
    let mut irq_settings = IrqSettings::default();
    irq_settings.enable_tap_irq(
        TapRecognitionMode::Single,
        XYZ {
            x: true,
            y: true,
            z: true,
        },
        InterruptRoute::Int1,
        false,
    );
    let settings = LsmSettings::basic().with_irq(irq_settings);
    let mut imu = LSM6DS3TR::new(spi_interface).with_settings(settings);
    imu.init().expect("Couldn't initialize the LSM6 sensor!");

    let reachable = imu.is_reachable();
    if reachable.is_ok() {
        loop {
            info!(
                r#"LSM6 readings:
            Accelerometer  scaled | {:?}
            Gyroscope      scaled | {:?}
            Temperature    scaled | {:?}"#,
                imu.read_accel(),
                imu.read_gyro(),
                imu.read_temp(),
            );
            if let Ok(irqs) = imu.read_interrupt_sources() {
                if !irqs.is_empty() {
                    let mut s = String::new();
                    for irq in irqs.iter() {
                        s.push_str(&format!("{:?}, ", irq));
                    }
                    info!("Interrupts ({}): [{:?}]", irqs.len(), s.as_str());
                }
            } else {
                warn!("error in reading irqs");
            }
            delay.delay_ms(500);
        }
    }

    loop {
        delay.delay_ms(1000);
    }
}
