#![no_std]
#![no_main]

use defmt::*;
use defmt_rtt as _;
use hal::Timer;
use panic_halt as _;
use rand::rngs::SmallRng;
use rand::RngCore;
use rand::SeedableRng;
use rp_pico::entry;
use rp_pico::hal;
use rp_pico::hal::pac;
use rp_pico::hal::prelude::*;
use smart_leds::{SmartLedsWrite, RGB8};
use ws2812_pio::Ws2812;

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    let mut watchdog = hal::Watchdog::new(pac.WATCHDOG);

    let clocks = hal::clocks::init_clocks_and_plls(
        rp_pico::XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

    let sio = hal::Sio::new(pac.SIO);

    let pins = rp_pico::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let timer = Timer::new(pac.TIMER, &mut pac.RESETS);

    let (mut pio, sm0, _, _, _) = pac.PIO0.split(&mut pac.RESETS);
    let mut ws = Ws2812::new(
        pins.gpio6.into_mode(),
        &mut pio,
        sm0,
        clocks.peripheral_clock.freq(),
        timer.count_down(),
    );

    let mut small_rng = SmallRng::seed_from_u64(0xDEADBEEFDEADBEEF);
    const LEN: usize = 54; //* 6 sides of 9 LEDs each to form a cube
    let mut leds: [RGB8; LEN] = [(0, 0, 0).into(); LEN];

    info!("Disco!");
    loop {
        for _ in 0..256 {
            for led in &mut leds {
                let score: u8 = small_rng.next_u32() as u8 % 3;
                let mut red: u8 = 0;
                let mut green: u8 = 0;
                let mut blue: u8 = 0;
                match score {
                    0 => red = 255,
                    1 => green = 255,
                    2 => blue = 255,
                    _ => (),
                };
                *led = (red, green, blue).into();
            }
            ws.write(leds.iter().copied())
                .expect("Couldn't write to WS LEDs chain!");
            delay.delay_ms(16);
        }
    }
}
